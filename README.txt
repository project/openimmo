CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

The OpenImmo data standard is mainly used in the german speaking Europe.
OpenImmo describes an real estate object in a set of over 300 fields.
It uses the XML Data Format.
http://www.openimmo.de

REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 /admin/config/services/openimmo